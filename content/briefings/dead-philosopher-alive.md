---
title: "One Voice Does Not Fit All. Conceptualize, create, design and build Virtual Representative(s)"
date: 2024-05-23T00:00:00+01:00
publishdate: 2024-05-23T00:00:00+01:00
image: "/img/posts/briefings.jpg"
tags: ["briefing"]
type: "post"
summary: "Design a virtual respresentative for a non-human entity (examples
below) that can both represent its source material and can adjust its
behavior and appearance to various relevant target audiences in order to
bridge the gap between those two groups."
comments: false
status: closed
---

**Design challenge:**

> Design a virtual respresentative for a non-human entity (examples
> below) that can both represent its source material and can adjust its
> behavior and appearance to various relevant target audiences in order to
> bridge the gap between those two groups.

As an example, an avatar that:
- can represent the content of an online community such as a
neighbourhood group with its many different opinions\
- convey the right information in text, appearance and behavior to city
counsil, journalists, and people from within the community,

But a similar system could represent: other communities (e.g. reddit
groups), research groups, organizations and brands in general.

**Moderating information and content, mediating people and information
by design and development of virtual representatives.** Virtual
representatives, avatars, chatbots and synthetic humans powered by
generative a.i. and large language models like GPT open up possibilities
for moderating content and information and for mediating and
representing real people, brands, companies, institutions or
organisations in new ways to a wide variety of target audiences. The
research group Interaction Design (IXD) is interested in the shape,
matter, and power of our relation with information. In particular, we
see how humans struggle in the face of information overload. These new
technologies might be able to mitigate, mediate and moderate phenomenon
related to information overload.

**With this as an outset: we are interested in study and design
(research into) creating virtual representative(s) to mitigate
information overload for domains of our interest like** education,
journalism, science communication, libraries, magazines, arts and
cultural exchange, museums, publishing and so on. Your interface and
interaction concepts and design experiment(s) determines and defines how
virtual representatives deal with information flows, could look, appear,
operate, behave and function and determines visual design / shape and
form of these 'non-human entities'.

**HBO-i domains:** Software Engineering, User Interaction

### IT-Areas: Media Design, Smart Mobile, Game Design, web development (front-end/back-end), UX design, Artificial Intelligence.

### Context

We live in the information age. The research group Interaction Design
(IXD) is interested in the shape, matter, and power of our relation with
information. In particular, we see how humans struggle in the face of
information overload:

-   Quantity of information (too much or not enough),
-   Quality of information (information density, ease of use of info),
-   Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard,
1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload)),
but the technologies surrounding us are. Do those new technologies and
the products built around them help us deal with information overload or
do they only make things worse?

We (hereby) pose strategic design experiments for ideas, concepts,
products, services, and environments with regards to new ways of
information moderation, transformation, curation, and publishing by
means of (generative) AI.

Part of the experiments is to critically look at the (potential) impact
(negative and positive) of such technologies and ideas on democracy,
healthcare, education, journalism, arts and cultural exchange, museums,
publishing, built environments, and crossovers.

**This critical view is essential because it is dubious whether new
services such as generative AI are really making us more creative, more
productive, smarter, happier, work better as a society.**

Currently, we are pursuing projects that deal with:

-   enticing intellectual and creative pursuits
-   moderating info-overload instead of contributing
-   mediating people using virtual representatives

## 

## The Assignment

### Problem / Opportunity / Challenge

**Mediating people and information by design and development of virtual
representatives.** Virtual representatives, avatars, chatbots and
synthetic humans powered by generative a.i. and large language models
like GPT open up possibilities for moderating content and information
and for mediating and representing real people or organisations.
Interface and interaction concepts and design determines and defines how
they could look, appear, operate, behave and function and determines
visual design / shape and form of these 'entities'. (from abstract
representation and conventional interface design to realistically
rendered interfaces that look like humans or game like characters or
anything in between or beyond)

The DPBTSE is interested in those kinds of representations for their own
platform but also for other applications in domains like democracy,
healthcare, education, journalism, arts and cultural exchange / museums,
(digital)publishing. Cases were a lot of information for a variety of
target audiences might or needs to be automatically processed and
(re)presented in a lasting appealing way to save time and resources of
real people representation or to become better than the real thing.

New technologies for moderating and mediating information by NLP holds
promises and potential as well as challenges. Soon we might be
overflowed with low grade chatbots, avatars, synthetic humans and the
like. Can we make lasting and beautiful interactions instead.

### 

### Assignment

Develop designs and design alternatives and prototypes for virtual
representatives for the DPBTSE given the content and substance and the
current look and feel of the DPBTSE. We are interested in a design study
and evaluation of visual and character possibilities with regard to
virtual representation of the DPBTSE.

To be further developed to a preferred design(s) and working
prototype(s).

### 

### Research / creative opportunities / requirements

-   Experiment and research into the interaction (design) of virtual
    representatives (look and feel, appearance, shape, form, operation,
    behavior and function) that are driven by generative a.i. / LLM's
    like GPT.
-   Study and conceptualize and or design possible implementation and
    application in a variety of media and on several devices possibly.
-   Research into the impact of generative a.i. / natural language
    processing on media, interaction and interface design.
-   UXD / IXD of artificial intelligence in general and more specific
    the visual appearance and behavior of interfaces and
    representatives.
-   Research into and shaping of personal assistants. Ethics and
    aesthetics.
-   Research into interface and interaction trends and developments
    directly or indirectly related to the advent of generative a.i. and
    LLM''s like GPT.
-   Research into generative artificial intelligence application
    ethically and aesthetically.

### 

### Guidance {#guidance-1}

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is
part of the research group Interaction Design (IXD) of Fontys ICT. The
DPBTSE is a combination of a thinktank and a creative lab for the
research group interested in working with the combination of applied:

-   Sciences,
-   Arts (and design),
-   Creativity and Philosophy, to create innovative interactive
    concepts, products, services and experiments therefore.

The project is open ended, meaning we will adapt and respond to
interesting intermediate results more than that we have a set target. We
need adventurous students that are willing to get engaged with the
researchers, leading to a potentially very rewarding internship.

**Your contacts are:**

-   Olaf Janssen, PhD in computational physics, experience in mobile app
    development, web develoment and design, AI and LLM, game design and
    development.

-   John van Litsenburg, teacher and developer of UXD / IXD strategy,
    concepts, product and services development and 2d / 3d design and of
    societal impact of artificial intelligence. Work and worked for
    Media design, Smart Mobile, Artificial intelligence.

Feel free to contact with any question about the internship assignment.
