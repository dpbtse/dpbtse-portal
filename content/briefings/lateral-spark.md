---
title: "Lateral Spark: An augmented reading experience to enhance creativity and problem solving"
date: 2024-05-23T00:00:00+01:00
publishdate: 2024-05-23T00:00:00+01:00
image: "/img/posts/briefings-2.jpg"
tags: ["briefing"]
type: "post"
summary: "Create a lateral thinking augmentation system for creative professionals to stimulate innovative ideas and solve unrelated problems while consuming existing text."
comments: false
status: closed
---

**Design challenge:**

> Create a **lateral thinking augmentation system** for **creative
> professionals** to **stimulate innovative ideas and solve unrelated
> problems while consuming existing text**.

The challenge can be approached from different perspectives and can be
adjusted to the IT-Area of interest of the student:

**HBO-i domains:** Software Engineering, User Interaction\
**IT-Areas:** AI engineering, web development (front-end/back-end), UX
design

## Context

The research group Interaction Design (IXD) is interested in the shape,
matter, and power of our relationship with information. In particular,
we see how humans struggle in the face of information overload:

-   Quantity of information (too much or not enough),
-   Quality of information (information density, ease of use of info),
-   Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard,
1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload)),
but the technologies surrounding us are. Do those new technologies and
the products built around them help us deal with information overload or
do they only make things worse?

We (hereby) pose strategic design experiments for ideas, concepts,
products, services, and environments with regards to new ways of
information moderation, transformation, curation, and publishing by
means of (generative) AI.

Part of the experiments is to critically look at the (potential)
positive and negative impact of such technologies and ideas on
democracy, healthcare, education, journalism, arts and cultural
exchange, museums, publishing, built environments, and crossovers.

**This critical view is essential because it is dubious whether new
services such as generative AI are really making us more creative, more
productive, smarter, happier, work better as a society.**

Currently, we are pursuing projects that deal with:

-   enticing intellectual and creative pursuits
-   moderating info-overload instead of contributing
-   mediating people using virtual representatives

## The Assignment

### Problem

In today\'s information-rich environment, we are often called out to
think out-of-the-box and come up with innovative ideas. Because this is
hard, and we are pressed for time, it is tempting to let an AI generate
ideas for us. While the quality of the generated ideas will no doubt
improve over time, using this technology may offload creative thinking
instead of stimulating it.

We would like to be able to use the strengths of generative AI in
lateral thinking (it can easily make connections between two unrelated
concepts, try it) to genuinely stimulate creative thoughts in the user
instead of taking this meaningful activity away from them. Lateral
thinking involves approaching problems from new and unexpected angles,
often requiring a break from linear thought processes.

### Assignment

Develop a system that enhances lateral thinking by augmenting existing
text (e.g., fiction, non-fiction, articles) with prompts, questions, or
interactive elements designed to stimulate innovative ideas about an
unrelated problem the user is working on.

We\'re trying to stimulate the process that leads to the sudden ideas
you get while taking a shower or a walk or doing another unrelated
activity.

To do this you need to use new LLM technologies to analyze the text
being consumed by the user to identify key themes, concepts, and
narratives and link them to the unrelated user\'s challenge. Based on
the analysis, generate thought-provoking prompts, questions, or
activities that encourage the user to think about their unrelated
problem in new ways. These prompts should be subtly integrated into the
reading experience without disrupting the flow. Maybe the original text
could be altered?

### Research opportunities

-   Use LLM concepts such as smart prompting, text embeddings, and
    semantic space to analyze the content of texts to identify links
    between an existing texts and unrelated challenges
-   Develop algorithms for augmented reading interventions
-   Explore the psychological and cognitive impact of augmented reading
    on creative problem-solving.
-   Assess the user experience and engagement with the system through
    user testing and feedback.
-   Study the (long-term) effects of using such a system on creativity
    and problem-solving abilities.
-   As with all projects, we invite you to think critically of the
    possible impact of anything you create.

### Expected outcome

Depending on the experience and interest of the student, we envision:

-   an AI-assisted system that takes (part of) the text and generates
    text augmentations based on a pre-determined unrelated user defined
    challenge

-   (web) platform that offers a pleasant reading experience of existing
    reading material (perhaps epubs) and includes the AI-generated
    interventions or augmentations

-   user experience tests and validation whether such a system may
    enhance (long-term) lateral thinking skills

### Guidance

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is
part of the research group Interaction Design (IXD) of Fontys ICT. The
DPBTSE is a combination of a thinktank and a creative lab for the
research group interested in working with the combination of applied:

-   Sciences,
-   Arts (and design),
-   Creativity and Philosophy, to create innovative interactive
    concepts, products, services and experiments.

The project is open ended, meaning we will adapt and respond to
interesting intermediate results more than setting a clear target. We
need adventurous students that are willing to get engaged with the
available researchers, leading to a potentially very rewarding
internship.

Your contacts are:

-   Olaf Janssen, PhD in computational physics, experience in mobile app
    development, web develoment and design, AI and LLMs, game design and
    development.

-   John van Litsenburg, teacher and developer of UXD / IXD strategy,
    concepts, product and services development and 2d / 3d design and of
    societal impact of artificial intelligence. Work and worked for
    Media design, Smart Mobile, Artificial intelligence.

Feel free to contact with any question about the internship assignment.
