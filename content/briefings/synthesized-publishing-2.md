---
title: "SynthPub - an AI curated digital garden"
date: 2024-12-03T00:00:00+01:00
publishdate: 2024-12-03T00:00:00+01:00
image: "/img/posts/briefings-2.jpg"
tags: ["briefing"]
type: "post"
summary: "Improve an automated publishing platform for content creators and consumers that publishes and curates itself like a digital garden to reduce information overload."
comments: false
---

**Design challenge:**

> Improve an **automated publishing platform** for **content creators and consumers** that **publishes and curates itself like a digital garden** to **reduce information overload.**

**HBO-i domains:** Software Engineering, User Interaction.
**IT-Areas:** Media Design, Smart Mobile, Artificial Intelligence. And combinations
### Context

We live in the information age. The research group Interaction Design (IXD) is interested in the shape, matter, and power of our relation with information. In particular, we see how humans struggle in the face of information overload:

- Quantity of information (too much or not enough),
- Quality of information (information density, ease of use of info),
- Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard, 1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload "https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload")), but the technologies surrounding us are. Do those new technologies and the products built around them help us deal with information overload or do they only make things worse?

**We (hereby) pose strategic design experiments for ideas, concepts, products, services, and environments with regards to new ways of information moderation, transformation, curation, and publishing by means of (generative) AI.**

Part of the experiments is to critically look at the (potential) impact (negative and positive) of such technologies and ideas on democracy, healthcare, education, journalism, arts and cultural exchange, museums, publishing, built environments, and crossovers.

**This critical view is essential because it is dubious whether new services such as generative AI are really making us more creative, more productive, smarter, happier, work better as a society.**

**Research / creative opportunities / requirements**

- Experiment and research into the interaction (design) of virtual representatives (look and feel, appearance, shape, form, operation, behavior and function) that are driven by generative a.i. / LLM’s like GPT.
- Study and conceptualize and or design possible implementation and application in a variety of media and on several devices possibly.
- Research into the impact of generative a.i. / natural language processing on media, interaction and interface design.
- UXD / IXD of artificial intelligence in general and more specific the visual appearance and behavior of interfaces and representatives.
- Research into and shaping of personal assistants. Ethics and aesthetics.
- Research into interface and interaction trends and developments directly or indirectly related to the advent of generative a.i. and LLM’’s like GPT.
- Research into generative artificial intelligence application ethically and aesthetically.

## The Assignment

### Problem
With the rise of generative AI, automatic publishing platforms will become ubiquitous. Such platforms will no doubt contribute to information overload by constantly generating new content without regard for quality, relevance, or redundancy. This leads to a fragmented information landscape where valuable insights are buried under repetitive or low-value posts. Can we apply the techniques of automated publishing to let substance win over content, meaning quality of quantity?

Instead of seeing an automated publishing platform as a content generating machine to continuously grab the attention of users with minimal effort and news, we invite you to envision an automated publishing platform that treats its content as tending a digital garden: prune less relevant information, refocus on new and relevant information. Can such a platform be a rewarding base to return to for users with an interest in a particular topic?

### Assignment

Improve the current prototype of an automated publishing platform that synthesizes, curates, and refines information to create a dynamic and coherent knowledge base. The platform could:

Aggregate content automatically from various input sources, such as RSS feeds,  blogs, or news sites. Then, it could identify its relation to a particular topic or target audience and be able to create an automated post. Instead of continuously creating new posts, it could refine old posts, or even remove old posts, based on new information. The system could become a digital garden, constantly tended to ensure the information remains relevant and accurate, just like Wikipedia pages are maintained by its user community. You can also explore adding user interaction to refine the platform's curation process, and explore how for instance visuals can be generated for the post.
### Research opportunities
Depending on the background of the student and the state of the existing prototype at the start of the internship, progress can be made towards a more scalable system, better interface design, AI curator system improvements.

- Investigate techniques for automated content generation and curation using modern LLM tools and frameworks
- Explore the impact of continuous content refinement on information quality and user satisfaction.
- Study user interaction patterns with synthesized content and their contributions to the refinement process.
- As with all projects, we invite you to think critically of the possible impact of anything you create and what this should mean for the developed platform.

### Guidance

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is part of the research group Interaction Design (IXD) of Fontys ICT. The DPBTSE is a combination of a thinktank and a creative lab for the research group interested in working with the combination of applied:

- Sciences,
- Arts (and design),
- Creativity and Philosophy, to create innovative interactive concepts, products, services and experiments therefore.

The project is open ended, meaning we will adapt and respond to interesting intermediate results more than that we have a set target. We need adventurous students that are willing to get engaged with the researchers, leading to a potentially very rewarding internship.

**Your contacts are:**

- Olaf Janssen, PhD in computational physics, experience in mobile app development, web development and design, AI and LLM, game design and development.
- John van Litsenburg, teacher and developer of UXD / IXD strategy, concepts, product and services development and 2d / 3d design and of societal impact of artificial intelligence. Work and worked for Media design, Smart Mobile, Artificial intelligence.

Feel free to contact with any questions, input or ideas about the internship assignment.