---
title: "Synthesized Publishing"
date: 2024-05-23T00:00:00+01:00
publishdate: 2024-05-23T00:00:00+01:00
image: "/img/posts/briefings-2.jpg"
tags: ["briefing"]
type: "post"
summary: "Create an automated publishing platform for content creators and consumers that publishes and curates itself like a digital garden to reduce information overload."
comments: false
status: closed
---

**Design challenge:**

> Create an **automated publishing platform** for **content creators and
> consumers** that **publishes and curates itself like a digital
> garden** to **reduce information overload.**

The challenge can be approached from different perspectives and can be
adjusted to the IT Area of interest of the student with an interest in:

**HBO-i domains:** Software Engineering, User Interaction\
**IT-Areas:** AI engineering, web development (front-end/back-end), UX
design

## Context {#context-1}

The research group Interaction Design (IXD) is interested in the shape,
matter, and power of our relationship with information. In particular,
we see how humans struggle in the face of information overload:

-   Quantity of information (too much or not enough),
-   Quality of information (information density, ease of use of info),
-   Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard,
1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload)),
but the technologies surrounding us are. Do those new technologies and
the products built around them help us deal with information overload or
do they only make things worse?

We (hereby) pose strategic design experiments for ideas, concepts,
products, services, and environments with regards to new ways of
information moderation, transformation, curation, and publishing by
means of (generative) AI.

Part of the experiments is to critically look at the (potential)
positive and negative impact of such technologies and ideas on
democracy, healthcare, education, journalism, arts and cultural
exchange, museums, (digital) publishing and others.

**This critical view is essential because it is dubious whether new
services such as generative AI are really making us more creative, more
productive, smarter, happier, work better as a society.**

Currently, we are pursuing projects that deal with:

-   enticing intellectual and creative pursuits
-   moderating info-overload instead of contributing
-   mediating people using virtual representatives

## The Assignment

### Problem

With the rise of generative AI, automatic publishing platforms will pop
up everywhere. Such platforms will no doubt contribute to information
overload by constantly generating new content without regard for
quality, relevance, or redundancy. This leads to a fragmented
information landscape where valuable insights are buried under
repetitive or low-value posts. Can we apply the techniques of automated
publishing to let substance win over content, meaning quality of
quantity?

Instead of seeing an automated publishing platform as a content
generating machine to continuously grab the attention of users with
minimal effort and news and potentially false information, we invite you
to envision an automated publishing platform that treats its content as
tending to a digital garden. Can such a platform be a rewarding base to
return to for users with an interest in a particular topic?

### Assignment

Develop an automated publishing platform that synthesizes, curates, and
refines information to create a dynamic and coherent knowledge base. The
platform could:

Aggregate content automatically from various input sources, such as RSS
feeds, blogs, or news sites. Then, it could identify its relation to a
particular topic or target audience and be able to create automated
posts. Instead of continuously creating new posts, it could refine old
posts, or even remove old posts, based on new information. The system
could become a digital garden, constantly tended to ensure the
information remains relevant and accurate, just like Wikipedia pages are
maintained by its user community. You can also explore adding user
interaction to refine the platform\'s curation process and explore how
for instance visuals can be generated for the post.

### Research opportunities

-   Setting up an automated publishing platform using static site
    generators
-   Investigate techniques for automated content generation and curation
    using modern LLM tools and frameworks
-   Explore the impact of continuous content refinement on information
    quality and user satisfaction.
-   Study user interaction patterns with synthesized content and their
    contributions to the refinement process.
-   As with all projects, we invite you to think critically of the
    possible impact of anything you create.

### Expected outcome

Depending on the experience and interest of the student, we envision:

-   a system that uses CI/CD and static site generators to be a
    constantly updated online presence

-   AI-assisted system to read, generate, and curate old and new posts
    from incoming information streams

-   a template, or blueprint, or tutorial on how to set up such a system
    for a given topic and target audience

### Guidance

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is
part of the research group Interaction Design (IXD) of Fontys ICT. The
DPBTSE is a combination of a thinktank and a creative lab for the
research group interested in working with the combination of applied:

-   Sciences,
-   Arts (and design),
-   Creativity and Philosophy, to create innovative interactive
    concepts, products, services and experiments.

The project is open ended, meaning we will adapt and respond to
interesting intermediate results more than setting a clear target. We
need adventurous students that are willing to get engaged with the
available researchers, leading to a potentially very rewarding
internship.

Your contacts are:

-   Olaf Janssen, PhD in computational physics, experience in mobile app
    development, web develoment and design, AI and LLM, game design and
    development.

-   John van Litsenburg, teacher and developer of UXD / IXD strategy,
    concepts, product and services development and 2d / 3d design and of
    societal impact of artificial intelligence. Work and worked for
    Media design, Smart Mobile, Artificial intelligence.

Feel free to contact with any question about the internship assignment.
