---
title: "Develop concepts, design alternatives and prototypes for virtual digital representative(s) for the D.P.B.T.S.E. given the content and substance and the current look and feel of the D.P.B.T.S.E."
date: 2024-12-03T00:00:00+01:00
publishdate: 2024-12-03T00:00:00+01:00
image: "/img/posts/briefings.jpg"
tags: ["briefing"]
type: "post"
summary: "Conceptualize and design (alternative) virtual respresentatives for a non-human entity, in this case The DPBTSE that can both represent its source material and can possibly adjust for instance its content, behavior and appearance to various relevant target audiences on different devices as to inform and entice them for mattes of The DPBTSE."
comments: false
---

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is part of the research group Interaction Design (IXD) of Fontys ICT. The DPBTSE is a combination of a startup thinktank and a creative lab for the research group. We use and promote applied arts, applied science and applied philosophy to inform, conceptualize, design and develop. We pose strategic design experiments for ideas, concepts, products, services, and environments with regards to new ways of information moderation, transformation, curation, and publishing by means of (generative) AI.

**Currently, we are pursuing projects that deal with:**

- enticing intellectual and creative pursuits
- moderating info-overload instead of contributing
- mediating and informing people using virtual / digital representatives

See: dpbtse.com for an impression.

**HBO-i domains:** Software Engineering, User Interaction.
**IT-Areas:** Media Design, Smart Mobile, Game Design, Artificial Intelligence. And combinations

### **ASSIGNMENT**

Conceptualize and design (alternative) virtual respresentatives for a non-human entity, in this case The DPBTSE that can both represent its source material and can possibly adjust for instance its content, behavior and appearance to various relevant target audiences on different devices as to inform and entice them for mattes of The DPBTSE.

Develop concepts and design alternatives and prototypes for virtual representative(s) for the DPBTSE given the content and substance and the current look and feel of the DPBTSE.

We are interested in a design study and evaluation of visual and character possibilities with regard to virtual representation of the DPBTSE and working prototypes of this.

The DPBTSE is interested in those kinds of representations for their own platform but also for other applications in domains like democracy, healthcare, education, journalism, arts and cultural exchange / museums, (digital)publishing. Cases where a lot of information for a variety of target audiences might or needs to be automatically processed and (re)presented in a lasting and appealing way.

New technologies for moderating and mediating information by NLP holds promises and potential as well as challenges. Soon we might be overflowed with low grade chatbots, avatars, synthetic humans and the like. Can we make lasting and beautiful interactions instead.

Your interface and interaction concepts and design experiment(s) determines and defines how virtual representatives deal with information flows, could look, appear, operate, behave and function and determines visual design / shape and form of these ’non-human entities'.

**Moderating information and content, mediating people and information by design and development of virtual representatives.** Virtual representatives, avatars, chatbots and synthetic humans powered by generative a.i. and large language models like GPT open up possibilities for moderating content and information and for mediating and representing real people, brands, companies, institutions or organisations in new ways to a wide variety of target audiences. The research group Interaction Design (IXD) is interested in the shape, matter, and power of our relation with information. In particular, we see how humans struggle in the face of information overload. These new technologies might be able to mitigate, mediate and moderate phenomenon related to information overload.

**Your contacts are:**

- John van Litsenburg, teacher and developer of UXD / IXD strategy, concepts, product and services development and 2d / 3d design and of societal impact of artificial intelligence. Work and worked for Media design, Smart Mobile, Artificial intelligence.
- Olaf Janssen, PhD in computational physics, experience in mobile app development, web development and design, AI and LLM, game design and development.

Feel free to contact with any question about the internship assignment.

### **SOME CONTEXT**

We live in the information age. The research group Interaction Design (IXD) is interested in the shape, matter, and power of our relation with information. In particular, we see how humans struggle in the face of information overload:

- Quantity of information (too much or not enough),
- Quality of information (information density, ease of use of info),
- Complexity or Simplicity of information (too much or too little).

This struggle is not new ([Kierkegaard, 1840](https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload "https://www.historynewsnetwork.org/article/how-to-cope-with-information-overload")), but the technologies surrounding us are. Do those new technologies and the products built around them help us deal with information overload or do they only make things worse?

**We (hereby) pose strategic design experiments for ideas, concepts, products, services, and environments with regards to new ways of information moderation, transformation, curation, and publishing by means of (generative) AI.**

Part of the experiments is to critically look at the (potential) impact (negative and positive) of such technologies and ideas on democracy, healthcare, education, journalism, arts and cultural exchange, museums, publishing, built environments, and crossovers.

**This critical view is essential because it is dubious whether new services such as generative AI are really making us more creative, more productive, smarter, happier, work better as a society.**

**Research / creative opportunities / requirements**

- Experiment and research into the interaction (design) of virtual representatives (look and feel, appearance, shape, form, operation, behavior and function) that are driven by generative a.i. / LLM’s like GPT.
- Study and conceptualize and or design possible implementation and application in a variety of media and on several devices possibly.
- Research into the impact of generative a.i. / natural language processing on media, interaction and interface design.
- UXD / IXD of artificial intelligence in general and more specific the visual appearance and behavior of interfaces and representatives.
- Research into and shaping of personal assistants. Ethics and aesthetics.
- Research into interface and interaction trends and developments directly or indirectly related to the advent of generative a.i. and LLM’’s like GPT.
- Research into generative artificial intelligence application ethically and aesthetically.

### **Guidance**

The DPBTSE (The Dead Philosophers Brainstorm To Solve Everything) is part of the research group Interaction Design (IXD) of Fontys ICT. The DPBTSE is a combination of a thinktank and a creative lab for the research group interested in working with the combination of applied:

- Sciences,
- Arts (and design),
- Creativity and Philosophy, to create innovative interactive concepts, products, services and experiments therefore.

The project is open ended, meaning we will adapt and respond to interesting intermediate results more than that we have a set target. We need adventurous students that are willing to get engaged with the researchers, leading to a potentially very rewarding internship.

**Your contacts are:**

- John van Litsenburg, teacher and developer of UXD / IXD strategy, concepts, product and services development and 2d / 3d design and of societal impact of artificial intelligence. Work and worked for Media design, Smart Mobile, Artificial intelligence.
- Olaf Janssen, PhD in computational physics, experience in mobile app development, web develoment and design, AI and LLM, game design and development.

Feel free to contact with any questions, input or ideas about the internship assignment.