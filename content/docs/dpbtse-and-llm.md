---
title: "THE DPBTSE and LLM an incomplete intro"
date: 2024-04-25T00:00:00+01:00
publishdate: 2024-04-25T00:00:00+01:00
image: "/img/thumbs/thumb_fiber.jpeg"
tags: ["project", "document"]
type: "post"
summary: "A dada-ish document proto-manifesto that tries to note a couple of important notions pertaining to the application of LLM’s and generative a.i. Apart from technical possibilities and challenges there are important notions and considerations regarding application of LLM's ethically and aesthetically."
comments: false
---

A dada-ish document proto-manifesto that tries to note a couple of important notions pertaining to the application of LLM’s and generative a.i. 
 
Apart from technical possibilities and challenges there are important notions and considerations regarding application of LLM's ethically and aesthetically. 

> **Read its current status in: [The DPBTSE and LLM an incomplete intro.pdf](/documents/The-DPBTSE-and-LLM-an-incomplete-intro.pdf).**
