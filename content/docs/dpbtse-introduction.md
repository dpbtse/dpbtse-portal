---
title: "THE DPBTSE - an incomplete intro"
date: 2023-12-05T00:00:00+01:00
publishdate: 2023-12-05T00:00:00+01:00
image: "/img/thumbs/thumb_tardi.jpeg"
tags: ["project", "document"]
type: "post"
summary: "For an incomplete intro on The DPBTSE we offer the super duper intro on The one and only Dead Philosophers Brainstorm to Solve Everything including a couple of ‘things to die for’."
comments: false
---

We hereby introduce The D.P.B.T.S.E. A
proposal and initiative for experiment with a hybrid thinktank and
creative studio-lab entity to establish much wanted and needed new
21st century combined-creative-and-intellectual-space.

> **For further information and inspiration download the: [The D.P.B.T.S.E. an incomplete intro.pdf](/documents/The-DPBTSE-an-incomplete-intro.pdf).**

**The D.P.B.T.S.E. 'The Dead Philosophers Brainstorm To Solve
Everything' is a prototype and a proto-brand for this. The DPBTSE is a
hybrid of a story-world and a real-world game and application. Inside
and outside F-ICT.**

**ONCE UPON A TIME. The DPBTSE. THINKTANK OF THINKTANKS part A.
Introduction.**

The DPBTSE is an adventurous bottom-up initiative and experiment that
transcends traditional academic, artistic, but also socio-economic
boundaries. By stimulating open--ended creativity and holistic thinking,
through integration and combination of the Arts, Science and Philosophy,
collaboration between humans and AI, and through development of
interdisciplinary and unconventional approaches to the most interesting
ideas, promises, opportunities, challenges and issues of our 21^st^
century (synthetic and biological) information -and communication age
and of interaction -and media design at large. Inside and outside F-ICT.

The DPBTSE is building platform and community as a thinktank and
creative engine in the same move. And in fact, is asked to explore such
possibility. We are convinced this is a great opportunity to be
innovative and establish (brand) value for F-ICT and the
Research Group IXD. Nationally and internationally. One of the outsets and
desires. By having courage to doing things in a novel unconventional way
in both form and content.

The DPBTSE is a hybrid type of initiative. Partly Thinktank. Partly
Studio. Partly School. Partly Company. Partly Game. Partly Research.
Partly Experiment. Partly incubator. Partly concept. Partly story.
Partly organism. Partly (science) fiction. Partly Pseudonym. Partly
mind.

The DPBTSE is a platform for promoting and deploying creativity, open
ended creation and exploration, experimental human-human and
human-machine collaborations.



**To be able to anticipate the future you best \'make\' the future.**

Overarching hypothesis and ideal of The DPBTSE is that there is a new
world of creative and intellectual potential that is now neglected and
wasted in all of us by a single-minded culture dominated with a
suffocating obsession for econometrics, money and growth, problem
identification and solving, closed concrete objectives, metrics and
measurable targets as opposed to the proposed framework of knowledge,
understanding, wisdom, informed open ended exploration, play,
imagination, and a much more universal creation philosophy.

**FUTURE-ICT.**

**The DPBTSE / Art / Science / Philosophy / Media / Education.**

![Sub1](/img/posts/introduction_sub_1.jpg)

**Introducing the D.P.B.T.S.E.**

**ONCE UPON A TIME. The DPBTSE. THINKTANK OF THINKTANKS part B.
Inception**.

**For this purpose, The DPBTSE envisions an adventurous project and
research experiment that transcends traditional academic and artistic as
well as mediatic, generational and socio-economic boundaries.**

First of all. by promoting and using principles, experience, knowledge,
inspiration and modes of working from the Arts, Sciences and Philosophy
in an integrated way, The DPBTSE is interested in developing new
intellectual and (creative) paradigms, structuring and enriching
holistic thinking -and creative processes and in co-creating outputs of
new mediatic forms. By promoting and deploying creativity, open ended
creation and exploration, experimental human-human and human-machine
collaborations. And by promoting of -and deploying interdisciplinary
approaches to - the promises, opportunities and challenges of our
21st-century (synthetic and biological) information and communication
age and to interaction and media design at large. Inside and outside
FICT. We are interested in empathy with past, present and future.


Second of all. By promoting and integrating generalist and specialist
knowledge with creative talent and process, by promoting and focusing on
open ended divergence, exploration, thinking -and creation. And by
promoting and deploying interdisciplinary approaches and experimental
human-human and human-machine collaborations, The DPBTSE is convinced to
expand on existing creative -and intellectual potential as well as to
explore the promises and opportunities, responsibilities, issues and
challenges of our 21st-centuries information and communication age as
well as of (ICT) education and of interaction -and media design. At
large!

**We think humanities way of mining and integrating creative and
intellectual potential thus far is analogues to a man with a fork in a
world of soup.** - Anonymous living philosopher (aspiring member).

![Sub1](/img/posts/introduction_sub_2.jpg)

**Introducing the D.P.B.T.S.E.**

**ONCE UPON A TIME. The DPBTSE. THINKTANK OF THINKTANKS part C.
Conception.**

The DPBTSE is (absurdly) ambitious in creating research, experiment and
progress through systematic use of knowledge, creativity and imagination
and through (self) perpetuation and multiplication of The DPBTSE as well
as spinoffs.

We are interested in cooperation and experiment in media, publishing,
concept, applied art, science and philosophy, education, profit and
non-profit entrepreneurship. In opensource economy and in startups and
spinoffs based on the very mission and vision of The DPBTSE. By teachers
and students. And by (existing and new) parties in and outside FICT.

We want to operate in and outside of the Lectorate IXD and in and
outside of F-ICT.


![Sub1](/img/posts/introduction_sub_3.jpg)

**Introducing the D.P.B.T.S.E.**

**ONCE UPON A TIME. The DPBTSE. THINKTANK OF THINKTANKS part D. Doing
things differently.**

[The DPBTSE is a hybrid type of initiative. We are a meta-team. We are
transmediatic.]{.ul}

Partly Thinktank. Partly Studio. Partly School. Partly Company. Partly
Game. Partly Research. Partly Experiment. Partly incubator. Partly
concept. Partly story. Partly organism. Partly (science) fiction. Partly
Pseudonym. Partly future. Partly mind. Partly life. Partly automaton.

The DPBTSE is interested in societal, economic and educational
innovation and reform. The DPBTSE is interested in Protopia.

- We are furthermore interested in creating engaging stories and
narratives as vehicles for creation and change.
- We are and propose hereby a mixed or alternate reality as an
experimental vehicle for education and engagement.
- We are experienced and knowledgeable do-ers and thinkers and appreciate
structured but open-ended non-linear ways of working.
- We are interested in imagination and creativity but not just for the
sake of it.
- We are of course interested in the Arts, Sciences and Philosophy.

![Sub1](/img/posts/introduction_sub_4.jpg)

**Introducing the D.P.B.T.S.E.**

**ONCE UPON A TIME. The DPBTSE. THINKTANK OF THINKTANKS part E. Reaching
out.**

The DPBTSE has 'set up first shop' and is now open for ideas,
cooperation or consultation!

- We are in the midst of natural and cultural evolution (if not facing a
revolution to come).
- Sometimes the best solutions are the ones you were not looking for.
- The best way to predict the future is by making it.
- What we were, are and will become time will perhaps tell us.
- Sometimes lack of knowledge is good for creativity. A romantic notion
of the late nineteenth century. Most of the time it is not and it will
only get us killed.

**Call to action. What can you do?**

If you feel attracted to these ideas and outsets and want to support
them, please let us know. You can respond in general, from your
professional and personal role and perspective or otherwise.

- We are open (ended) and looking for what you want to share with us.
Nothing if not critical.
- We are looking for friends, further endorsement ambassadors and
resources for creating momentum.
- We are now open (ended) for and looking for your comments and ideas and
your take on possibilities. Of any kind.
- We are looking for further foundation and optimal organizational form.
Foundation? Explorative Ambition theme?
- We are open (ended) for talk or consult for anybody from the Lectorate
IXD.
- We are open (ended) and looking for a limited number of interesting
challenges and cooperations for this experiment or a part of it.
- We have reached out to you as we think we might have interesting things
to do together. We know however that you are busy.
- Your idea for collaboration or your quest might be a The D.P.B.T.S.E.
challenge.

![Sub1](/img/posts/introduction_sub_5.jpg)

**Introducing the D.P.B.T.S.E.**

**ONCE UPON A TIME. The DPBTSE. THINKTANK OF THINKTANKS part E. Reaching
out.**

Note: The DPBTSE is interested in the good life and needs to engage in
creative adventure with an (absurd) humorous touch.

On the other hand, we are dead serious.

**The DPBTSE.**

Contact.

Aspiring<sup>\*</sup> member. Olaf Janssen.  
Aspiring<sup>\*</sup> member. John van Litsenburg.

Or contact us via teams.

Website and social media coming.

<sup>\*</sup>Living philosophers can be aspiring members only.

![Sub1](/img/posts/introduction_sub_6.jpg)
