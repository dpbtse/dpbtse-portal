---
title: "The DPBTSE creative engine / network and process scheme.  Connecting the intellectual and the creative space."
date: 2024-03-12T00:00:00+01:00
publishdate: 2024-03-12T00:00:00+01:00
image: "/img/thumbs/thumb_wheel.jpeg"
tags: ["project", "document"]
type: "post"
summary: "This schematic is a first 2d layout for human machine cooperation and creative and intellectual endeavour of The DPBTSE in conjunction with the research group IXD agenda and ideas."
comments: false
---

This schematic is a first 2d layout for human machine cooperation and creative and intellectual endeavour of The DPBTSE in conjunction with the research group IXD agenda and ideas.
 
It is a mixture of a process-scheme, an illustration of the dpbtse philosophy in conjunction with IXD, the forming of a body of knowledge and portfolio and of
artificial intelligently powered thinking and creating and automated publishing of outputs.

{{< load-photoswipe >}}
{{< gallery >}}
  {{< figure src="/documents/DPBTSEscheme.jpg" >}}
  {{< figure src="/documents/DPBTSEscheme2.jpg" >}}
{{< /gallery >}}
