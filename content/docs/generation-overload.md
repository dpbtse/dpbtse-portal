---
title: "Generation overload. Info overload theme. Proto magazines. This document is an assembly of first thoughts and notes pertaining to info-overload."
date: 2024-04-20T00:00:00+01:00
publishdate: 2024-04-20T00:00:00+01:00
image: "/img/thumbs/thumb_magazine.jpeg"
tags: ["project", "document"]
type: "post"
summary: "This document was a first attempt for content and substance of a proto-magazine / proto-manifesto on info overload and a first set up of re-thinking our relationship with information in the information age part two; the age of artificial intelligence."
comments: false
---

This document was a first attempt for content and substance of a proto-magazine / proto-manifesto on info overload and a first set up of re-thinking our relationship with information in the information age part two; the age of artificial intelligence.
 
Liberal democratic societies are ever more complex and demand evermore information exchange or moderations. Where we want to educate (in the broadest sense of the word) and inform people this is a big challenge especially given the sheer quantity of information that might overflow us.

> **Read its current status in: [Information overload proposal magazine.pdf](/documents/Information-overload-proposal-magazine.pdf).**

