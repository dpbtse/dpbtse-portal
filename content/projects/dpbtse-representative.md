---
title: "DPBTSE virtual representative"
date: 2024-02-03T00:00:00+01:00
publishdate: 2024-02-03T00:00:00+01:00
image: "/img/thumbs/thumb_hanzi.jpeg"
tags: ["project", "prototype"]
type: "post"
summary: "A prototype to represent ourselves as a prototype for general virtual representatives - communicating pluriform internal data to all kinds of external parties."
comments: false
---

A prototype to represent ourselves as a prototype for general virtual representatives - communicating pluriform internal data to all kinds of external parties.

![Sub1](/img/posts/virtual_rep_1.jpeg)

Our virtual Representation Platform prototype:
- Can consult data from notes or knowledge bases (e.g. Obsidian)
- Can consult data from socials (e.g. MS Teams channel chats, Mastodon, RSS feeds)
- Can use tools to look up knowledge and present summaries to different perspectives/world views, or link recent events for deeper insights

<iframe style="width:100%;height:360px;" src="https://www.youtube-nocookie.com/embed/VLdveEUDf1E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

But imagine, how we can use this to represent any digital community that can make its own community feel represented but can also effective communicate its intentions and feelings to journalists, policy makers, and other AIs.

![Sub1](/img/posts/virtual_rep_2.jpeg)
![Sub1](/img/posts/virtual_rep_3.jpeg)

