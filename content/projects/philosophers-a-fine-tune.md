---
title: "Philosophers - a fine tune"
date: 2022-12-14T00:00:00+01:00
publishdate: 2022-12-14T00:00:00+01:00
image: "/img/thumbs/thumb_brain.jpeg"
tags: ["project", "prototype"]
type: "post"
summary: "We fine-tuned a custom GPT3 model with Alfred North Whitehead's *Process & Reality*, and another one with Baruch Spinoza's *Ethics*. Two fine-tuned models tuned to talk like Spinoza or Whitehead, now put tête-à-tête in a discussion. It is clear that we must be careful, fine-tuned models not only talk like them they 'read' like them."
comments: false
---

We fine-tuned a custom GPT3 model with Alfred North Whitehead's *Process & Reality*, and another one with Baruch Spinoza's *Ethics*. Two fine-tuned models tuned to talk like Spinoza or Whitehead, now put tête-à-tête in a discussion. It is clear that we must be careful, fine-tuned models not only talk like them they 'read' like them.

The topic is: *How come humanity seems on a constant path towards self-destruction?*

> **Baruch Spinoza:** "I think that the reason for this appears to be that the human mind builds too many hypotheses, and that it is not sufficiently convinced of the truth of those propositions which are true, and sufficiently persuaded of the falsity of those which are false, so that it can pass quickly from one to the other."

> **Alfred North Whitehead:** "And what is the remedy for this defect? Spinoza says that the mind should lose its hesitations, and should have a high confidence in itself. So you think that the remedy is the loss of self-confidence? What is wrong with the confidence in oneself? What other confidence is there? … The confidence in oneself is the confidence in its own fundamental intuitions which lie at the basis of the unity of the world. The mind loses its self-confidence when it doubts whether its own fundamental intuitions are in fact true."

> **Baruch Spinoza:** "If you mean that the mind should not have any doubt about the truth of fundamental intuitions, then I agree. But what do you mean by having no doubt about the truth of fundamental intuitions? Is it not merely doubt about the truth of some of its own intuitions? I do not understand what you mean."

> **Alfred North Whitehead:** "It is only when the mind doubts of its own fundamental intuitions that it can even begin the process of inquiry into the conformation of its intuitions to the facts."

> **Baruch Spinoza:** "Just because the mind doubts about fundamental intuitions does not imply that it doubts about the whole realm of its own intuitions; for, as I have shown, the mind doubts of the whole realm of its own intuitions only as far as the imagination extends, and this only in so far as the mind's own fundamental intuitions have been modified by the imagination. The mind doubts, in other words, about the whole of its own intuitions, not about those referred to the imagination."

> **Alfred North Whitehead:** "But the mind is not a vacuum; it presupposes the community. If it doubts its own fundamental intuitions, then, obviously, it also doubts its presuppositions. But, in the first place, this is only a particular case of doubt, and not doubt about doubt. It is only when the mind is conscious that its presuppositions are such as they are that it can doubt of its presuppositions. In other words, conscious doubt is a stage in the process of conscious belief. Secondly, how can the presuppositions be doubted of if they are not doubted of?… The mind only doubts of its presuppositions when it is conscious that it doubts."

> **Baruch Spinoza:** "I do not see how the mind can doubt of its presuppositions as long as it believes in them with a sufficient confidence."

> **Alfred North Whitehead:** "One presupposition cannot be doubted without the whole scheme of presuppositions being doubted. This is immediate belief. … You cannot have a doubt in the absence of an affirmative belief. In the case of immediate belief, the belief is merely negative,the mind does not believe in the presupposition, but does not doubt it. … The mind is conscious of its implicit presupposition, and doubts of its implicit presupposition as a step in the process of belief. … You cannot have a doubt which does not lead to some addition to belief."

> **Baruch Spinoza:** "A mind which is conscious of the false in its fundamental intuitions, and which doubts of its true intuitions is not a mind which knows anything. But a mind which doubts of its true intuitions is a mind which knows nothing. … I am far from wishing to say that a mind which doubts of its true intuitions is equally ignorant with a mind which doubts of its false intuitions; but, in so far as doubt is a process which consists in the modification of the mind by the imagination, the mind which has such a doubt is a mind which is ignorant of the true, as well as of the false."

