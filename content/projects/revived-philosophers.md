---
title: "Revived philosophers in the News"
date: 2024-01-15T00:00:00+01:00
publishdate: 2024-01-15T00:00:00+01:00
image: "/img/posts/revived_schop.jpeg"
tags: ["project", "prototype"]
type: "post"
summary: "Using prompt chaining and tool usage we can bring dead philosophers back to life, have them read RSS news feeds, browse our DPBTSE notes and inject some joie-de-vivre into meme-worthy presence."
comments: false
---

Using prompt chaining and tool usage we can bring dead philosophers back to life, have them read RSS news feeds, browse our DPBTSE notes and inject some joie-de-vivre into meme-worthy presence.

![Sub1](/img/posts/revived_adorno.jpeg)
![Sub1](/img/posts/revived_sontag.jpeg)
![Sub1](/img/posts/revived_arendt.jpeg)

