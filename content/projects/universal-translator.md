---
title: "Universal Translator/Transformer"
date: 2024-05-20T00:00:00+01:00
publishdate: 2024-05-20T00:00:00+01:00
image: "/img/thumbs/thumb_babel.jpeg"
tags: ["project", "prototype"]
type: "post"
summary: "A proposal for a universal translator by application of LLM. We are presenting an approach involving dissecting and analyzing text through a hierarchical stack of modalities ranging from core meaning to complex sociocultural and psychological layers, integrating concepts from multiple scientific disciplines and methodologies."
comments: false
---

A proposal for a universal translator by application of LLM. We are presenting 
an approach involving dissecting and analyzing text through a hierarchical 
stack of modalities ranging from core meaning to complex sociocultural and 
psychological layers, integrating concepts from multiple scientific disciplines 
and methodologies.

This multidisciplinary approach doesn’t fit neatly into a single existing category 
but spans several areas of study. To define the underlying science or 
methodology we call it “Computational Semiotics within Multimodal 
Discourse Analysis.”

> **For further information and inspiration download the: [Computational Semiotics within Multimodal Discourse Analysis.pdf](/documents/Computational-Semiotics-within-Multimodal-Discourse-Analysis.pdf).**
